<!-- Autentikasi -->
<?php require('../config.php');
    if(is_logged_in()){
?>

<!-- Proses Chat -->
<?php
	if (isset($_POST['submit'])){
		// Attempt MySQL server connection.
		require('config.php');

		// Receive recipient info
		$nama_terima= $_REQUEST['customer'];

		//Get username
		$nama_kirim= $_SESSION['username'];
		
		// Escape user input for security
		$pesan= mysqli_real_escape_string($conn, $_REQUEST['pesan']);

		// Get date
		date_default_timezone_set('Asia/Jakarta');
		$tanggal= date('y-m-d H:i');

		// Attempt insert query execution
		$sql = "INSERT INTO chat (nama_terima, nama_kirim, pesan, tanggal)
				VALUES ('$nama_terima', '$nama_kirim', '$pesan', '$tanggal')";
		if(mysqli_query($conn, $sql)){
			;
		} else{
			echo '<script language="javascript">
			alert ("Pesan tidak terkirim! \n Mohon periksa kembali koneksi anda.");
			</script>';
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Halaman Chat</title>
	<style type="text/css">
		#chat-user{
			color: black;
			position: relative;
			text-align: left;
			left: 6px;
			top: 40px;
			font-size: 25px;
			font-family: montserrat semibold;
		}

		#table-box{
			width: 958px;
			height: 529px;
			box-shadow: 0 3px 10px rgba(0, 0, 0, 0.16);
			border-radius: 30px;
			background-color: white;
			position: relative;
			display: inline-block;
			text-align: left;
			top: 90px;
			padding: 50px 0 0 25px;
		}

		#search-bar {
			width:272px;
			height:40px;
			border-radius: 30px;
			border: 1px solid #BBBBBB;
			text-align: left;
			color: #797979;
			font-size: 12px;
			font-family: montserrat;
			position: absolute;
			padding: 0 0 0 42px;
		}

		#search-icon {
			position: absolute;
			z-index: 1;
			top: 61px;
			left: 40px;
		}

		.garis-tengah {
			border: none;
			height: 529px;
			width: 4px;
			position: absolute;
			top: 0;
			left: 330px;
			background-color: #F0F0F0;
		}

		table {
			position: relative;
			border-collapse: collapse;
			text-align: center;
			top: 65px;
		}
		tr {
			transition: 0.3s;
			cursor:pointer;
		}
		tr:hover {
			background-color: #F0F0F0;;
		}
		td {
			height: 70px;
			padding: 0 0 0 5px;
		}
		td.profile {
			width: 60px;
		}
		td.name {
			width: 222px;
			font-family: montserrat medium;
			font-size: 16px;
			text-align: justify;
		}

		#profilepic{
			width: 50px;
			height: 50px;
			text-align: center;
			vertical-align: middle;
		}

		#garis-batas{
			width: 282px;
			height: 1px;
			border: none;
			border-radius: 50%;
			background-color: #797979;
			position: absolute;
			padding: 0;
			margin: 0;
		}

		/* Kotak Chat */
		.chat{
			width: 626px;
			height: 529px;
			background-color: white;
			position: absolute;
		  	display: none;
			left: 0;
			top: 0;
			padding: 0;
			margin: 0;
			overflow: hidden;
		}
		.chat:target {
			display: table;
			position: absolute;
			top: 0;
			left: 334px;
			text-align: center;
			border-top-right-radius: 30px;
			border-bottom-right-radius: 30px;
			overflow: hidden;
		}
		#chat-goket{
			position: absolute;
			text-align: center;
			top: 215px;
			left: 469px;
			color: #969696;
		}
		#chat-header{
			background-color: #F0F0F0;
			width: 626px;
			height: 60px;
			position: relative;
			top: 0;
			border-top-right-radius: 30px;
			padding: 18px;
			font-size: 20px;
			font-family: montserrat semibold;
			text-decoration: underline;
		}
		#chatbar{
			width: 626px;
			height: 60px;
			background-color: #F0F0F0;
			border: none;
			position: absolute;
			bottom: 0;
			border-bottom-right-radius: 30px;
		}
		.message-bar{
			width: 450px;
			height: 40px;
			border: 1px solid #969696;
			font-size: 16px;
			font-family: montserrat medium;
			border-radius: 30px;
			padding: 0 0 0 25px;
			margin-right: 15px;
		}
		.message-send{
			padding: 10.5px 13px;
			border-radius: 30px;
			background-color: #E05B36;
			font-size: 16px;
			font-family: montserrat semibold;
			transition: 0.3s;
			color: white;
			border: none;
		}
		.message-send:hover{
			outline: 3px solid #E05B36;
			color: #E05B36;
			background-color: white;
			outline-offset: -3px;
			cursor: pointer;
		}

		/* CSS for message bubbles and chat box */
		.chatbox{
			position: absolute;
			top: 60px;
			width: 631px;
			height: 409px;
			overflow: auto;
		}
		.messagebubble{
			padding: 15px;
			max-width: 60%;
			display: inline-block;
			text-align: left;
			border-radius: 20px;
			font-size: 16px;
			font-family: montserrat medium;
			margin-top: 5px;
			margin-bottom: 5px;
		}
		#send{
			border: none;
			outline: 2px solid #969696;
			outline-offset: -2px;
			margin-right: 25px;
			float: right;
			clear: both;
		}
		#receive{
			border: none;
			background-color: #e0d74c;
			margin-left: 25px;
			clear: both;
		}

		#waktu{
			font-family: montserrat medium;
			font-size: 12px;
			margin: 15px 0 0 0;
			color: #707070;
			text-align: right;
		}

		#chathist{
			text-align: left;
		}

	</style>
</head>
<body>
	<?php $this_page='chat'; ?> <!-- Ini nama pagenya supaya bisa active di navbar -->
	<!-- Untuk sidebar, header dan content disambung mulai dari sini. -->
	<?php require("../page_template.php"); ?>

		<!--Sambungan dari div class:"content" dari page_template dan tutupnya juga disini-->
		<div class="col12" id="page-content"> <!-- Intinya style dari id="page-content" tapi tanpa padding -->
			<div class="row">
				<div id="chat-user">
					<p style="display: inline; font-family: montserrat medium; margin: 0;">Chat </p><p style="color: #E05B36; display: inline; margin: 0;"><?php echo $_SESSION['username'];?></p>
				</div>

				<!-- Tabel Chat -->
				<div class="col12" style="position: absolute; top: 0; text-align: left;">
					<div id="table-box">
						<!-- Form input untuk search -->
						<div>
							<form>
								<img src="search-icon.svg" id="search-icon">
								<input id= "search-bar" type="text" name="search" placeholder="Cari pelanggan">
							</form>
						</div>

						<!-- Chat -->
						
						<!-- Tabel Chat -->
						<table>
						<?php
						require ('config.php');

						$query = "SELECT id_cust, customer FROM customer_chat";
						$rs_result = mysqli_query($conn, $query);
						$batas_garis = mysqli_num_rows($rs_result);

						//Untuk penempatan garis
						$awal = 185;
						$tambah = 0;
						$jlh_garis = 0;

						while ($row = mysqli_fetch_array($rs_result)) {
							$tambah = $awal + $tambah;
							$awal = 70;
						?>
							<form action="chat.php?customer=<?php echo $row['customer'];?>#mulai-chat" method="POST">
								<tr onclick="location.href='chat.php?customer=<?php echo $row['customer'];?>#mulai-chat'">
									<td class="profile"><img src="profile-pic.svg" id="profilepic"></td>
									<td class="name"><?php echo $row['customer'];?></td>
								</tr>
							</form>
						<?php
							$jlh_garis++;
							if ($jlh_garis < $batas_garis) {
								echo "<hr style='top: " . $tambah . "px;' id='garis-batas'>";
							}
						}
						?>
						</table>

						<!-- Garis Batas Chat -->
						<!-- <hr id= "garis-batas"> -->

						<!-- Goket ayo mulai chat -->
						<div id="chat-goket">
							<p style="color: #E05B36; font-size: 48.83px;font-style: montserrat; margin: 0; padding: 0;"><b>Goket</b></p>	
							<p style="font-size: 20px;font-style: montserrat medium; margin: 0; padding: 0;">Mulai obrolanmu</p>
							<p style="font-size: 16px;font-style: montserrat medium; margin: 0; padding: 0;">Silakan memilih pengguna untuk memulai</p>
						</div>

						<!-- Garis pemisah pengguna dengan chatbox -->
						<div class="garis-tengah"></div>

						<!-- Chat -->
						<div id="mulai-chat" class="chat" onload="show_func()">
							<!-- Chat Header -->
							<div id="chat-header"><?php echo $_GET['customer'];?></div>

							<!-- Pesan disini -->
							<script>
							// Function untuk show scroll chatnya
							function show_func(){
								var element = document.getElementById("chathist");
								element.scrollTop = element.scrollHeight;
							}
							</script>
							<div class="chatbox" id="chathist">
								<!-- PHP untuk menampilkan chat -->
								<?php
								//Nama pelanggan dalam chat
								$cust = $_GET['customer'];
								$user = $_SESSION['username'];

								//Memanggil row dalam sql dimana nama pelanggan yang dipilih ada dalam kolom nama_kirim atau nama_terima
								$query_chat = "SELECT * FROM chat WHERE (nama_kirim= '$cust' OR nama_terima= '$cust') AND (nama_kirim= '$user' OR nama_terima= '$user')";
								$run = mysqli_query($conn, $query_chat);

								while($row = $run->fetch_assoc()){
									//Jika pengirim adalah user, maka bubble chatnya akan berada di sisi kiri
									if($row['nama_kirim'] == $_SESSION['username']){
								?>
								<div style="clear: both;"></div>
								<div class="messagebubble" id="send">
									<p style="margin: 0;"><?php echo $row['pesan'];?></p>
									<p id="waktu"><?php echo $row['tanggal'];?></p>
								</div>
								<?php
									} else {?>
								<div style="clear: both;"></div>
								<div class="messagebubble" id="receive">
									<p style="margin: 0;"><?php echo $row['pesan'];?></p>
									<p id="waktu"><?php echo $row['tanggal'];?></p>
								</div>
								<?php
									}
								}?>
							</div>

							<!-- Chat input -->
							<div id="chatbar">
								<form action="chat.php?customer=<?php echo $_GET['customer'];?>#mulai-chat" method="POST" style="position: relative; top: 10px;">
									<input class="message-bar" name="pesan" id="pesan" placeholder="Silahkan masukkan pesan Anda">
									<input class="message-send" type="submit" name="submit" id="submit" value="Kirim">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- Ini tutupnya div class:"content" sambungan dari page_template.php dan JANGAN DIHAPUS -->
</body>
</html>

<!-- Autentikasi -->
<?php  
} else {
    header('Location: ../index.php');
}