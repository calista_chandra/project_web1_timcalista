<!-- Autentikasi -->
<?php require('../config.php');
    if(is_logged_in()){
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tentang Goket</title>
	<style type="text/css">
		#message-box{
			width: 820px;
			height: 245px;
			box-shadow: 0 3px 10px rgba(0, 0, 0, 0.16);
			border-radius: 30px;
			background-color: white;
			position: relative;
			display: inline-block;
			text-align: center;
			top: 96px;
		}
	</style>
</head>
<body>
	<?php $this_page='tentang-goket'; ?> <!-- Ini nama pagenya supaya bisa active di navbar -->
	<!-- Untuk sidebar, header dan content disambung mulai dari sini. -->
	<?php require("../page_template.php"); ?>

		<!--Sambungan dari div class:"content" dari page_template dan tutupnya juga disini-->
		<div class="col12" style="background-color: #FAFAFA; height: 100%; margin: 0; padding: 0; position: relative;"> <!-- Intinya style dari id="page-content" tapi tanpa padding -->
			<div class="col12" style="background-color: #EFD660; height: 219px; position: relative;">
				<img src="e-commerce.svg" style="position: relative; top: 20px;">
			</div>
			<div class="col12">
				<p style="font-size: 25px;font-family: montserrat medium; position: relative; top: 50px; text-align: center; padding: 0; margin: 0;">Tentang Kami</p>
				<hr style="width: 112px;  position: relative; border-radius: 5px; border: 1px solid #E05B36; top: 51px; height: 3px; background-color:#E05B36; ">
			</div>
			<div class="col12">
				<div id="message-box">
					<p style="text-align: center; padding: 0; margin: 0; font-size: 16px; font-family: montserrat medium; position: relative; top: 56px;">Goket merupakan platform belanja online berfokus pada produk lokal. Kami
					<br>percaya dengan meningkatkan citra produk lokal, Indonesia dapat maju dan
					<br>berkembang.
					<br>
					<br>Berdiri sejak tahun 2022, Goket berdasar pada pelayanan yang cepat, mudah,
					<br>efektif, dan efisien bagi seluruh pengguna dengan berbagai metode
					<br>pembayaran yang dapat dipilih.</p>
				</div>
			</div>
		</div>
	</div> <!-- Ini tutupnya div class:"content" sambungan dari page_template.php dan JANGAN DIHAPUS -->
</body>
</html>

<!-- Autentikasi -->
<?php  } else {
    header('Location: ../index.php');
}