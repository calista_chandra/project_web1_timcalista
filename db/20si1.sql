-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2022 at 05:17 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `20si1`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(5) NOT NULL,
  `nama_kirim` varchar(50) NOT NULL,
  `nama_terima` varchar(50) NOT NULL,
  `pesan` varchar(200) NOT NULL,
  `tanggal` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `nama_kirim`, `nama_terima`, `pesan`, `tanggal`) VALUES
(12, 'Vallista LeoThames', 'Cangcimen', 'Halo', '22-04-05 17:25'),
(13, 'Cangcimen', 'Vallista LeoThames', 'Halo kak, adakah yang ingin kakak pesan?', '22-04-05 17:34'),
(14, 'Andrew', 'Cangcimen', 'Halo, selamat siang', '22-04-06 11:10'),
(15, 'Cangcimen', 'Andrew', 'Halo, selamat siang kak', '22-04-12 11:21'),
(16, 'Vallista LeoThames', 'Cangcimen', 'Saya ingin bertanya, untuk Ichiran Instant Noodle apakah ready kak?', '22-04-13 08:50'),
(17, 'Tifa', 'Cangcimen', 'Halo, apakah ada produk kentang goreng?', '22-04-13 09:25'),
(18, 'Cangcimen', 'Vallista LeoThames', 'Iya, produknya ready kak.', '22-04-13 09:28'),
(19, 'Vallista LeoThames', 'Cangcimen', 'Kira-kira berapa harganya untuk 2 pcs ya kak?', '22-04-13 09:29'),
(20, 'Cangcimen', 'Vallista LeoThames', 'Kalau untuk Ichiran Instant Noodle kira-kira 25k ya kak', '22-04-14 16:28');

-- --------------------------------------------------------

--
-- Table structure for table `customer_chat`
--

CREATE TABLE `customer_chat` (
  `id_cust` int(5) NOT NULL,
  `customer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_chat`
--

INSERT INTO `customer_chat` (`id_cust`, `customer`) VALUES
(1, 'Vallista LeoThames'),
(2, 'Andrew'),
(3, 'Tifa');

-- --------------------------------------------------------

--
-- Table structure for table `data_product`
--

CREATE TABLE `data_product` (
  `id` int(11) NOT NULL,
  `produk` varchar(50) DEFAULT NULL,
  `stok` varchar(10) DEFAULT NULL,
  `harga` varchar(50) NOT NULL,
  `deskripsi` text DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_product`
--

INSERT INTO `data_product` (`id`, `produk`, `stok`, `harga`, `deskripsi`, `foto`) VALUES
(22, 'Daging Kambing (100g)', '45', '40.000,00', 'Daging kambing kualitas tinggi.', '675-daging-kambing.svg'),
(23, 'Eureka Popcorn', '20', '30.000,00', 'Popcorn impor yang lezat!', '743-eureka-popcorn.svg'),
(24, 'Ichiran Instant Noodle', '440', '12.500,00', 'Mie impor dari Jepang!', '845-ichiran-instantnoodles.svg'),
(25, 'Susu Kambing Muda (500mL)', '86', '50.000,00', 'Susu kambing muda segar!', '948-susu-kambing.svg'),
(26, 'Wagyu A-5', '19', '350.000,00', 'Daging wagyu berkualitas tinggi', '161-wagyu.svg'),
(33, 'Pringles', '3', '40.000,00', 'Pringles rasa original', '604-pringles.png');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(5) NOT NULL,
  `pelanggan` varchar(50) NOT NULL,
  `produk` varchar(200) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `total` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('Baru','Siap Dikirim','Selesai') NOT NULL DEFAULT 'Baru',
  `stok_update` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`id`, `pelanggan`, `produk`, `jumlah`, `total`, `tanggal`, `status`, `stok_update`) VALUES
(1, 'Brian', 'Susu Kambing Muda (500mL)', 2, '100.000,00', '2022-04-12', 'Siap Dikirim', 0),
(2, 'Cecilia', 'Susu Kambing Muda (500mL)', 5, '250.000,00', '2022-04-01', 'Siap Dikirim', 0),
(3, 'Adit', 'Ichiran Instant Noodle', 5, '200.000,00', '2022-03-28', 'Selesai', 0),
(4, 'Jeffry', 'Wagyu A-5', 1, '350.000,00', '2022-03-16', 'Selesai', 0),
(5, 'Rani', 'Ichiran Instant Noodle', 3, '37.500,00', '2022-04-15', 'Siap Dikirim', 0),
(6, 'Justin', 'Wagyu A-5', 2, '700.000,00', '2022-04-17', 'Baru', 0),
(7, 'Budi', 'Daging Kambing (100g)', 5, '200.000,00', '2022-04-18', 'Baru', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ulasan`
--

CREATE TABLE `ulasan` (
  `nomor` int(5) NOT NULL,
  `pelanggan` varchar(50) NOT NULL,
  `produk` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL,
  `total` varchar(50) NOT NULL,
  `rating` int(1) NOT NULL,
  `tanggal` date NOT NULL,
  `komentar` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ulasan`
--

INSERT INTO `ulasan` (`nomor`, `pelanggan`, `produk`, `jumlah`, `total`, `rating`, `tanggal`, `komentar`) VALUES
(1, 'Yulianti', 'Susu Kambing Muda(500mL)', 2, '100.000,00', 2, '2021-12-10', 'Susunya kurang segar'),
(2, 'Carmen', 'Susu Kambing Muda(500mL)', 12, '600.000,00', 4, '2021-10-15', 'Susunya segar dan berkualitas'),
(3, 'Yuu Winata', 'Daging Kambing(100g)', 5, '200.000,00', 5, '2021-10-25', 'Daging segar berkualitas tinggi'),
(4, 'Jamet', 'Wagyu A-5', 1, '350.000,00', 4, '2021-09-16', 'Daging premiumnya bagus'),
(5, 'Gojou', 'Eureka Popcorn', 2, '60.000,00', 5, '2021-08-20', 'Enak Sekali'),
(6, 'Shogo', 'Ichiran Instant Noodle', 10, '125.000,00', 4, '2021-07-25', 'Bumbu mienya lumayan ya');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `pesanan_baru` int(3) NOT NULL,
  `pesanan_siapkirim` int(3) NOT NULL,
  `pesanan_selesai` int(3) NOT NULL,
  `ulasan_baru` int(3) NOT NULL,
  `produk_terjual` int(3) NOT NULL,
  `jumlah_pelanggan` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `no_hp`, `pesanan_baru`, `pesanan_siapkirim`, `pesanan_selesai`, `ulasan_baru`, `produk_terjual`, `jumlah_pelanggan`) VALUES
(1, 'Cangcimen', '7297e3154f3e208d26e94a265e7d911ae925e03b', '081278950090', 1, 2, 3, 6, 9, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_chat`
--
ALTER TABLE `customer_chat`
  ADD PRIMARY KEY (`id_cust`);

--
-- Indexes for table `data_product`
--
ALTER TABLE `data_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ulasan`
--
ALTER TABLE `ulasan`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `customer_chat`
--
ALTER TABLE `customer_chat`
  MODIFY `id_cust` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_product`
--
ALTER TABLE `data_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
